import React, {Component} from 'react';
import {Nav, Navbar, NavbarBrand, NavItem} from "reactstrap";
import {NavLink} from "react-router-dom";

class Navigation extends Component {
  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">Music app</NavbarBrand>
            <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink to="/">Artist page</NavLink>
                </NavItem>
            </Nav>
        </Navbar>
      </div>
    );
  }
}

export default Navigation;