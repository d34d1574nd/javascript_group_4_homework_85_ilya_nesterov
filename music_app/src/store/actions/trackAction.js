import axios from '../../axios-api';

export const FECTH_TRACK_SUCCESS = 'FECTH_TRACK_SUCCESS';

const fetchAlbumsSuccess = tracks => ({type: FECTH_TRACK_SUCCESS, tracks});

export const fetchTrack = idAlbum => {
  return dispatch => {
    return axios.get('/tracks?album=' + idAlbum).then(
      response => dispatch(fetchAlbumsSuccess(response.data))
    );
  };
};