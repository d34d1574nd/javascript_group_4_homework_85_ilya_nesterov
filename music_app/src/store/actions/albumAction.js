import axios from '../../axios-api';

export const FECTH_ALBUMS_SUCCESS = 'FECTH_ALBUMS_SUCCESS';

const fetchAlbumsSuccess = albums => ({type: FECTH_ALBUMS_SUCCESS, albums});

export const fetchAlbums = idArtist => {
  return dispatch => {
    return axios.get('/albums?artist=' + idArtist).then(
      response => dispatch(fetchAlbumsSuccess(response.data))
    );
  };
};