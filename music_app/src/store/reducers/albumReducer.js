import {FECTH_ALBUMS_SUCCESS} from "../actions/albumAction";

const initialState = {
  albums: []
};

const artistReducer = (state = initialState, action) => {
  switch (action.type) {
    case FECTH_ALBUMS_SUCCESS:
      return {...state, albums: action.albums};
    default:
      return state;
  }
};

export default artistReducer;