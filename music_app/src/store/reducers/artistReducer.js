import {FETCH_ARTISTS_SUCCESS} from "../actions/artistActions";

const initialState = {
  artist: []
};

const artistReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ARTISTS_SUCCESS:
      return {...state, artist: action.artist};
    default:
      return state;
  }
};

export default artistReducer;