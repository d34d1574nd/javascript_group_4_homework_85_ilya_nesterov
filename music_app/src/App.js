import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Switch, Route} from "react-router-dom";
import Navigation from "./components/Navigation/Navigation";
import ArtistPage from "./containers/ArtsitsPage/ArtistPage";
import AlbumPage from "./containers/AlbumPage/AlbumPage";
import TrackPage from "./containers/TrackPage/TrackPage";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header>
          <Navigation/>
        </header>
        <Container style={{marginTop: '20px'}}>
          <Switch>
            <Route path="/" exact component={ArtistPage}/>
            <Route path="/albums/:id/:artist" exact component={AlbumPage}/>
            <Route path="/tracks/:id/:album/:artist" exact component={TrackPage}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default App;
