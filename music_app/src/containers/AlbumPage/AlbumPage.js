import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchAlbums} from "../../store/actions/albumAction";
import {Button, Card, CardBody, CardText, CardTitle, Col, Row} from "reactstrap";
import ParamThumbnail from "../../components/ParamThumbnail/ParamThumbnail";
import {NavLink} from "react-router-dom";

class AlbumPage extends Component {
  componentDidMount() {
    this.props.getAblums(this.props.match.params.id);
  }

  goBack = () => {
    this.props.history.goBack('/');
  };

  render() {
    const album = this.props.album.map(albums => (
        <Col sm="4" key={albums._id}>
          <Card body>
            <strong>Artist</strong>
            <CardTitle>{albums.artist.name}</CardTitle>
            <ParamThumbnail
              param='album'
              image={albums.photoAlbum}
            />
            <CardBody>
              <strong>Album title</strong>
              <CardTitle>{albums.titleAlbum}</CardTitle>
              <strong>Year</strong>
              <CardText>{albums.year}</CardText>
              <NavLink to={`/tracks/${albums._id}/${albums.titleAlbum}/${albums.artist.name}`}>Go to tracks</NavLink>
            </CardBody>
          </Card>
        </Col>
      )
    );
    return (
      <Fragment>
        <Button color="link" onClick={this.goBack}>Go back</Button>
        <Row>
          {album}
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  album: state.album.albums
});

const mapDispatchToProps = dispatch => ({
  getAblums: idArtist => dispatch(fetchAlbums(idArtist))
});

export default connect(mapStateToProps, mapDispatchToProps)(AlbumPage);