import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchArtist} from "../../store/actions/artistActions";
import {Card, CardBody, CardLink, CardText, CardTitle, Col, Row} from "reactstrap";
import ParamThumbnail from "../../components/ParamThumbnail/ParamThumbnail";


class ArtistPage extends Component {

  componentDidMount() {
    this.props.getArtist()
  }

  render() {
    const artist = this.props.artist.map(artist => (
      <Col sm="3" key={artist._id}>
      <Card outline color="secondary">
        <ParamThumbnail
          param='artist'
          image={artist.photo}
        />
        <CardBody>
          <strong>Artist: </strong>
          <CardTitle>{artist.name}</CardTitle>
          <strong>Information: </strong>
          <CardText>{artist.information}</CardText>
          <CardLink href={`/albums/${artist._id}/${artist.name}`}>Albums</CardLink>
        </CardBody>
      </Card>
      </Col>
      )
    );
    return (
      <Row>
        {artist}
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  artist: state.artist.artist
});

const mapDispatchToProps = dispatch => ({
  getArtist: () => dispatch(fetchArtist())
});

export default connect(mapStateToProps, mapDispatchToProps)(ArtistPage);