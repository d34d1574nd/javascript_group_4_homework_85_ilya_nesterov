import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchTrack} from "../../store/actions/trackAction";
import {Button, Card, CardHeader, Col, Row} from "reactstrap";

class TrackPage extends Component {
  componentDidMount() {
    this.props.getTrack(this.props.match.params.id);
  }

  goBack = () => {
    this.props.history.goBack('/albums');
  };

  render() {
    const track = this.props.track.map(track => (
        <Col sm="8" key={track._id}>
          <Card body>
                <Row>
                  <Col sm="4"><strong>#{track.numberSong}</strong></Col>
                  <Col sm="4"><span>{track.titleSong}</span></Col>
                  <Col sm="4"><strong>{track.duration}</strong></Col>
                </Row>
          </Card>
        </Col>
      ));
    return (
      <Fragment>
        <Button  color="link" onClick={this.goBack}>Go back</Button>
        <Row>
          <Col  md="8">
            <Card>
              <CardHeader>
                <Row>
                  <Col sm="4">
                    <strong>artist: </strong>{this.props.match.params.artist}
                  </Col>
                  <Col sm="4">
                    <strong>album: </strong>{this.props.match.params.album}
                  </Col>
                </Row>
              </CardHeader>
            </Card>
          </Col>
        </Row>
        <Row>
          {track}
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  track: state.track.tracks
});

const mapDispatchToProps = dispatch => ({
  getTrack: idAlbum => dispatch(fetchTrack(idAlbum))
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackPage);