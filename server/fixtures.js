const mongoose = require('mongoose');
const nanoid = require('nanoid');

const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');
const TrackHistory = require('./models/TrackHistory');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for ( let collection of collections) {
    await collection.drop();
  }

  const [linkin, seconds, papaRoach] = await Artist.create(
    {name: 'Linkin Park', photo: 'linkinpark.jpeg', information: 'Group my love'},
    {name: '30 seconds to mars', photo: '30seconds.jpeg', information: 'My second love group'},
    {name: "Papa Roach", photo: "paparoach.jpeg", information: "My love group"}
  );

  const [hibrid, lie, war, metamorphos] = await Album.create(
    {
      titleAlbum: "Hibrid Theory",
      artist: linkin._id,
      year: "24 октября 2000 г.",
      photoAlbum: 'hibridLP.jpeg'
    },
    {
      titleAlbum: "A beautiful Lie",
      artist: seconds._id,
      year:  "30 августа 2005 г.",
      photoAlbum: "abeatifullie30.jpeg"
    },
    {
      titleAlbum: "This is war",
      artist: seconds._id,
      year: "8 декабря 2009 г.",
      photoAlbum: "thisisWar30.jpeg"
    },
    {
      titleAlbum: "Metamorphosis",
      artist: papaRoach._id,
      year: " 24 марта 2009 г.",
      photoAlbum: "Metamorphosis.jpeg"
    }
  );

  const track = await Track.create(
    {
      numberSong: 1,
      titleSong: "Papercut",
      album: hibrid._id,
      duration: '3:05'
    },
    {
      numberSong: 2,
      titleSong: "One step closer",
      album: hibrid._id,
      duration: '2:36'
    },
    {
      numberSong: 3,
      titleSong: "With you",
      album: hibrid._id,
      duration: '3:23'
    },
    {
      numberSong: 4,
      titleSong: "points of authority",
      album: hibrid._id,
      duration: '3:20'
    },
    {
      numberSong: 5,
      titleSong: "Crawling",
      album: hibrid._id,
      duration: '3:29'
    },
    {
      numberSong: 6,
      titleSong: "Runaway",
      album: hibrid._id,
      duration: "3:03"
    },
    {
      numberSong: 7,
      titleSong: "By myself",
      album: hibrid._id,
      duration: "3:10"
    },
    {
      numberSong: 8,
      titleSong: "in the end",
      album: hibrid._id,
      duration: '3:36'
    },
    {
      numberSong: 9,
      titleSong: 'a place for my head',
      album: hibrid._id,
      duration: '3:05'
    },
    {
      numberSong: 10,
      titleSong: 'Forgotten',
      album: hibrid._id,
      duration: '3:14'
    },
    {
      numberSong: 11,
      titleSong: "cure for the itch",
      album: hibrid._id,
      duration: '2:37'
    },
    {
      numberSong: 12,
      titleSong: "pushing me away",
      album: hibrid._id,
      duration: '3:12'
    },
    {
      numberSong: 1,
      titleSong: 'ATTACK',
      album: lie._id,
      duration: "3:09"
    },
    {
      numberSong: 2,
      titleSong: 'A Beautiful Lie',
      album: lie._id,
      duration: "4:05"
    },
    {
      numberSong: 3,
      titleSong: 'The Kill',
      album: lie._id,
      duration: '3:51'
    },
    {
      numberSong: 4,
      titleSong: "was it a dream?",
      album: lie._id,
      duration: "4:16"
    },
    {
      numberSong: 5,
      titleSong: "the fantasy",
      album: lie._id,
      duration: "4:29"
    },
    {
      numberSong: 6,
      titleSong: "Savior",
      album: lie._id,
      duration: '3:24'
    },
    {
      numberSong: 7,
      titleSong: 'From yesterday',
      album: lie._id,
      duration: "4:07"
    },
    {
      numberSong: 8,
      titleSong: "The story",
      album: lie._id,
      duration: "3:55"
    },
    {
      numberSong: 9,
      titleSong: "R-Evolve",
      album: lie._id,
      duration: "4:00"
    },
    {
      numberSong: 10,
      titleSong: 'a modern myth',
      album: lie._id,
      duration: '2:59'
    },
    {
      numberSong: 11,
      titleSong: 'battle of one',
      album: lie._id,
      duration: "2:47"
    },
    {
      numberSong: 12,
      titleSong: 'Hunter',
      album: lie._id,
      duration: "3:55"
    },
    {
      numberSong: 1,
      titleSong: 'escape',
      album: war._id,
      duration: '2:24'
    },
    {
      numberSong: 2,
      titleSong: 'night of the hunter',
      album: war._id,
      duration: '5:41'
    },
    {
      numberSong: 3,
      titleSong: 'kings and queens',
      album: war._id,
      duration: '5:47'
    },
    {
      numberSong: 4,
      titleSong: 'This is war',
      album: war._id,
      duration: '5:27'
    },
    {
      numberSong: 5,
      titleSong: '100 suns',
      album: war._id,
      duration: '1:58'
    },
    {
      numberSong: 6,
      titleSong: 'hurricane',
      album: war._id,
      duration: '6:12'
    },
    {
      numberSong: 7,
      titleSong: "closer to the edge",
      album: war._id,
      duration: '4:33'
    },
    {
      numberSong: 8,
      titleSong: 'vox populi',
      album: war._id,
      duration: '5:43'
    },
    {
      numberSong: 9,
      titleSong: 'search and destroy',
      album: war._id,
      duration: '5:38'
    },
    {
      numberSong: 10,
      titleSong: 'alibi',
      album: war._id,
      duration: '6:00'
    },
    {
      numberSong: 11,
      titleSong: 'stranger in a strange land',
      album: war._id,
      duration: '6:54'
    },
    {
     numberSong: 12,
     titleSong: 'l940',
     album: war._id,
     duration: '4:26'
    },
    {
      numberSong: 1,
      titleSong: "days of war",
      album: metamorphos._id,
      duration: '1:25'
    },
    {
      numberSong: 2,
      titleSong: "change or die",
      album: metamorphos._id,
      duration: '3:19'
    },
    {
      numberSong: 3,
      titleSong: 'hollywood whore',
      album: metamorphos._id,
      duration: '3:55'
    },
    {
      numberSong: 4,
      titleSong: 'i almost told you that i loved you',
      album: metamorphos._id,
      duration: '3:12'
    },
    {
      numberSong: 5,
      titleSong: 'lifeline',
      album: metamorphos._id,
      duration: '4:06'
    },
    {
      numberSong: 6,
      titleSong: 'enough',
      album: metamorphos._id,
      duration: '4:02'
    },
    {
      numberSong: 7,
      titleSong: 'live this down',
      album: metamorphos._id,
      duration: '3:36'
    },
    {
      numberSong: 8,
      titleSong: 'march out of the darkness',
      album: metamorphos._id,
      duration: '4:22'
    },
    {
      numberSong: 9,
      titleSong: 'into the light',
      album: metamorphos._id,
      duration: '3:28'
    },
    {
      numberSong: 10,
      titleSong: 'carry me',
      album: metamorphos._id,
      duration: '4:26'
    },
    {
      numberSong: 11,
      titleSong: 'nights of love',
      album: metamorphos._id,
      duration: '5:16'
    },
    {
      numberSong: 12,
      titleSong: 'state of emergency',
      album: metamorphos._id,
      duration: '5:07'
    }
  );

  const username = await User.create(
    {username: "Ilya", password: "1ly4", token: nanoid()},
    {username: "John", password: '12345', token: nanoid()}
  );

  await TrackHistory.create(
    {user: username[0]._id, track: track[6]._id, datetime: new Date().toISOString()},
    {user: username[0]._id, track: track[1]._id, datetime: new Date().toISOString()},
    {user: username[1]._id, track: track[9]._id, datetime: new Date().toISOString()}
  );

  return connection.close();
};

run().catch(error => {
  console.log(error)
});