const express = require('express');

const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
  const token = await req.get('Token');
  const user = await User.findOne({token});

  if(!user) {
    return res.status(401).send({error: "Authorization failed"})
  }

  const track = ({user: user._id, track: req.body.track, datetime: new Date().toISOString()});
  const trackHistory = await new TrackHistory(track);

  trackHistory.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));

  res.send({Authorized: "Authorization was successful"})
});

module.exports = router;