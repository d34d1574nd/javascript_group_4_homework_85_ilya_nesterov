const express = require('express');
const nanoid = require('nanoid');

const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
  const user = new User({username: req.body.username, password: req.body.password, token: nanoid(20)});

  try {
    await user.save();
    return res.send({token: user.token});
  } catch (error) {
    return res.status(400).send(error)
  }
});


router.post('/sessions', async (req, res) => {
  await User.updateOne({username: req.body.username}, {token: nanoid(20)});
  const user = await User.findOne({username: req.body.username});

  if (!user) {
    return res.status(400).send({error: 'Username incorrect'})
  }

  const isMatch = await user.checkPassword(req.body.password);

  if(!isMatch) {
    return res.status(400).send({error: "Password incorrect"})
  }

  res.send({token: user.token})
});



module.exports = router;